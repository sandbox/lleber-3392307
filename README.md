# Default Classes Filter
This module provides a filter plugin type that allows site builders to configure default classes to apply for arbitrary HTML elements.

## Requirements
This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module.
For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration
1. Enable the module at Administration > Extend (or more professionally, using config synchronization).
2. Add the filter to one or more text formats.
3. Configure the filter settings as appropriate.
