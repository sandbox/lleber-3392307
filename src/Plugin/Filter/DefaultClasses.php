<?php

namespace Drupal\default_classes_filter\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * @Filter(
 *   id = "default_classes",
 *   title = @Translation("Default classes"),
 *   description = @Translation("Adds default classes to various HTML elements."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "elements" = {
 *       "0" = {
 *         "tag" = "",
 *         "classes" = {
 *           "0" = "",
 *         },
 *       },
 *     },
 *   },
 * )
 */
class DefaultClasses extends FilterBase {

  /**
   * {@inheritDoc}
   */
  public function process($text, $langcode) {

    // Load the text into a document object model.
    $dom = Html::load($text);

    // Apply default classing for each configured element type.
    foreach ($this->settings['elements'] as $element) {
      foreach ($dom->getElementsByTagName($element['tag']) as $node) {
        /** @var \DOMElement $node */

        $classes = $element['classes'];

        // Merge in any classes that might already exist.
        if ($node->hasAttribute('class')) {
          $classes = array_merge(
            $classes,
            explode(' ', $node->getAttribute('class'))
          );
        }

        // Apply some security.  This might mangle some classes?
        $classes = array_map(static function($class) {
          return Html::getClass($class);
        }, $classes);

        // Dedupe.
        $classes = array_unique($classes);

        $node->setAttribute('class', implode(' ', $classes));
      }
    }
    return new FilterProcessResult(Html::serialize($dom));
  }

  /**
   *  {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;
    $form['elements'] = [
      '#type' => 'table',
      '#title' => $this->t('Elements'),
      '#prefix' => '<div id="elements">',
      '#suffix' => '</div>',
      '#header' => [
        $this->t('Tag'),
        $this->t('Classes'),
        $this->t('Operations'),
      ],
    ];

    $elements = $this->settings['elements'];
    foreach ($elements as $index => $element) {
      $form['elements'][$index] = [
        '#type' => 'details',
        '#title' => $element['tag'] ?: $this->t('New element'),
        '#open' => empty($element['tag']),
      ];
      $form['elements'][$index]['tag'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Tag'),
        '#title_display' => 'invisible',
        '#default_value' => $element['tag'],
      ];
      $form['elements'][$index]['classes'] = [
        '#type' => 'container',
        '#prefix' => '<div id="element-' . $index . '">',
        '#suffix' => '</div>',
      ];

      foreach ($element['classes'] as $class) {
        $form['elements'][$index]['classes'][] = [
          '#type' => 'textfield',
          '#title' => $this->t('Class'),
          '#title_display' => 'invisible',
          '#default_value' => $class,
        ];
      }
      $form['elements'][$index]['classes']['actions'] = [
        '#type' => 'actions',
      ];
      $form['elements'][$index]['classes']['actions']['add'] = [
        '#type' => 'submit',
        '#value' => $this->t('Add another class'),
        '#submit' => [static::class . '::ajaxRebuildClasses'],
        '#ajax' => [
          'callback' => static::class . '::addAnotherClass',
          'wrapper' => 'element-' . $index,
        ],
      ];
    }

    $form['elements'][$index]['operations'] = [
      '#type' => 'actions',
    ];

    $form['elements'][$index]['operations']['remove'] = [
      '#type' => 'submit',
      '#value' => $this->t('Remove'),
      '#name' => 'element-remove-' . $index,
      '#submit' => [static::class . '::ajaxRebuildElements'],
      '#ajax' => [
        'callback' => static::class . '::removeElement',
        'wrapper' => 'elements',
      ],
    ];

    $form['elements']['actions'] = [
      '#type' => 'actions',
    ];

    $form['elements']['actions']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add another element'),
      '#submit' => [static::class . '::ajaxRebuildElements'],
      '#ajax' => [
        'callback' => static::class . '::addAnotherElement',
        'wrapper' => 'elements',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    unset($configuration['settings']['elements']['actions']);
    foreach ($configuration['settings']['elements'] as &$element) {
      unset($element['classes']['actions']);
    }
    unset($element);
    return parent::setConfiguration($configuration);
  }

  /**
   * Submit handler that rebuilds the elements table.
   */
  public static function ajaxRebuildElements(array &$form, FormStateInterface $form_state) {
    return $form['filters']['settings']['default_classes']['elements'];
  }

  /**
   * Submit handler that rebuilds a class list.
   */
  public static function ajaxRebuildClasses(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();

    $parents = $triggering_element['#array_parents'];
    array_pop($parents); // Pop off the submit button.
    array_pop($parents); // Pop off the actions wrapper.

    $element_to_rebuild = $form;
    foreach ($parents as $parent) {
      $element_to_rebuild = $element_to_rebuild[$parent];
    }
    return $element_to_rebuild;
  }

  /**
   * AJAX callback that adds another element to the table.
   *
   * @TODO: Currently not working for some reason.
   */
  public static function addAnotherElement(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * AJAX callback that adds another class to a list.
   *
   * @TODO: Currently not working for some reason.
   */
  public static function addAnotherClass(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * AJAX callback that removes an element from the table.
   *
   * @TODO: Currently not working for some reason.
   */
  public static function removeElement(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }
}
